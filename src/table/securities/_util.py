import pandas as pd
from pandas import DataFrame


def normalize_general(df: DataFrame) -> DataFrame:
    """
    Takes any transformed market data & applies a final normalization
    """
    df.index.name = "date"
    for col in df.columns:
        df[col] = df[col].astype(float)
    df.index = pd.to_datetime(df.index)
    return df


def normalize_time_series(df: DataFrame) -> DataFrame:
    """
    Transforms raw time series data into common format
    """
    return normalize_general(
        df.rename(
            columns={
                "1. open": "open",
                "2. high": "high",
                "3. low": "low",
                "4. close": "closed",
                "5. volume": "volume",
            }
        )
    )


def normalize_crypto(df: DataFrame) -> DataFrame:
    """
    Transforms raw crypto data into common format
    """
    return normalize_general(
        df[
            [
                "1a. open (USD)",
                "2a. high (USD)",
                "3a. low (USD)",
                "4a. close (USD)",
                "5. volume",
            ]
        ].rename(
            columns={
                "1a. open (USD)": "open",
                "2a. high (USD)": "high",
                "3a. low (USD)": "low",
                "4a. close (USD)": "closed",
                "5. volume": "volume",
            }
        )
    )
