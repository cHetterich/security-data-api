from pandas import DataFrame
import requests
from . import _util


class AlphaVantageAPI:

    api_token: str

    def __init__(self, api_token: str):
        self.api_token = api_token

    def get_time_series_price(self, symbol: str, output_size: str) -> DataFrame:
        """
        Grabs the price of the given time series symbol. output size can be either `compact` for 100 days of data or `full` for all 20+ years of data
        """
        # Fetch data
        api_url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&outputsize={output_size}&apikey={self.api_token}"
        data = requests.get(api_url).json()

        # Transformed to normalized dataframe & return
        df = DataFrame(data["Time Series (Daily)"]).T
        return _util.normalize_time_series(df)

    def get_crypto_price(self, symbol: str, exchange: str) -> DataFrame:
        """
        Grabs the price of the given crypto symbol on the given exchange
        """
        # Fetch data
        api_url = f"https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol={symbol}&market={exchange}&apikey={self.api_token}"
        raw_df = requests.get(api_url).json()

        # Transformed to normalized dataframe & return
        df = DataFrame(raw_df["Time Series (Digital Currency Daily)"]).T
        return _util.normalize_crypto(df)
