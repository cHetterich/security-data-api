from dataclasses import dataclass, asdict
from datetime import datetime, timedelta
from typing import Tuple
from pandas import DataFrame


@dataclass
class ClientData:
    moment: datetime
    securities: list[tuple[str, float]]


class MockClient:

    _agg_client_data: DataFrame
    client_state: ClientData

    tick_speed: timedelta

    def __init__(self, tick_speed: timedelta, client_start_data: ClientData):
        self.tick_speed = tick_speed
        self._agg_client_data = DataFrame([client_start_data])
        self.client_state = client_start_data

    def tick(self):
        """
        Save snapshot of current client state & iterate current moment
        """
        self._agg_client_data = self._agg_client_data.append(
            asdict(self.client_state), ignore_index=True
        )
        self.client_state.moment = self.client_state.moment + self.tick_speed
